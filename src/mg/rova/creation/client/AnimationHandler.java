package mg.rova.creation.client;

import java.util.ArrayList;
import java.util.List;

public class AnimationHandler {

	protected final List<HandledAnimation> animations = new ArrayList<HandledAnimation>();
	protected int duration;
	protected int counter = 0;

	public AnimationHandler() {
		this(500);
	}

	public AnimationHandler(int animationDuration) {
		duration = animationDuration;
	}

	public void addAnimation(HandledAnimation animation) {
		animations.add(animation);
	}

	public void startAnimations() {
		if (counter >= animations.size())
			return;
		if (!animations.isEmpty())
			animations.get(counter++).animate(duration);
	}

	public void nextAnimation() {
		if (counter >= animations.size())
			return;
		if (!animations.isEmpty())
			animations.get(counter++).animate(duration);
	}

	public void reset() {
		counter = 0;
	}

	public void clearAnimations() {
		animations.clear();
		reset();
	}

}
