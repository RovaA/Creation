package mg.rova.creation.client.domain;

public class Person {

	protected String name;
	protected String firstName;
	protected int age;

	public Person(String name, String firstName, int age) {
		this.name = name;
		this.firstName = firstName;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
