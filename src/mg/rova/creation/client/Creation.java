package mg.rova.creation.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Creation implements EntryPoint {

	@Override
	public void onModuleLoad() {

		final AnimationHandler handler = new AnimationHandler();

		final VerticalPanel panel = new VerticalPanel();
		final Button button1 = new Button("1");
		new TranslateAnimation(button1.getElement()).animate(2500);
		panel.add(button1);
		final Button button2 = new Button("2");
		new TranslateAnimation(button2.getElement(), handler);
		panel.add(button2);
		final Button button3 = new Button("3");
		new TranslateAnimation(button3.getElement(), handler);
		panel.add(button3);
		final Button button4 = new Button("4");
		new TranslateAnimation(button4.getElement(), handler);
		panel.add(button4);
		final Button button5 = new Button("5");
		new TranslateAnimation(button5.getElement()).animate(500);
		panel.add(button5);
		final Button button6 = new Button("6");
		new TranslateAnimation(button6.getElement(), handler);
		new FadeAnimation(button6.getElement(), handler);
		panel.add(button6);
		RootLayoutPanel.get().add(panel);
		
		handler.startAnimations();

	}

	public native void ssc_scrollArray() /*-{
		if ($wnd.ssc_scrollArray)
			$wnd.ssc_scrollArray(null, null, null, null);

		alert($wnd.document);
	}-*/;
}
