package mg.rova.creation.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

public class AdvancedFlexTable<I> extends FlexTable {

	protected final List<Row> rows = new ArrayList<>();
	protected final List<Column<I, ?>> columns = new ArrayList<>();
	protected List<I> items = new ArrayList<>();

	public List<I> getItems() {
		return items;
	}

	public void addAllItems(List<I> items) {
		for (I item : items) {
			addItem(item);
		}
	}

	public void addItem(I item) {
		items.add(item);
		rows.add(new Row(item));
	}

	public List<Row> getRows() {
		return rows;
	}

	public List<Column<I, ?>> getColumns() {
		return columns;
	}

	public void addColumn(Column<I, ?> column) {
		columns.add(column);
	}

	public void refresh() {
		for (int i = 0; i < rows.size(); i++) {
			Row row = rows.get(i);
			for (int j = 0; j < columns.size(); j++) {
				Column<I, ?> column = columns.get(j);
				setWidget(i, j, column.renderedWidget(row.getItem()));
			}
		}
	}

	class Row {

		protected I item;

		public Row(I item) {
			this.item = item;
		}

		public I getItem() {
			return item;
		}

		public void setItem(I item) {
			this.item = item;
		}

	}

	public static abstract class Column<I, W extends Widget> {

		protected abstract W renderedWidget(I item);

	}

}
