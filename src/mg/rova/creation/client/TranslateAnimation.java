package mg.rova.creation.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;

public class TranslateAnimation extends HandledAnimation {

	public TranslateAnimation(Element element) {
		super(element, null);
	}

	public TranslateAnimation(Element element, AnimationHandler handler) {
		super(element, handler);
	}

	@Override
	public void init() {
		translate();
	}

	public void translate() {
		element.getStyle().setMarginLeft(2500, Unit.PX);
		cancel();
	}

	@Override
	protected void onUpdate(double progress) {
		doTranslate(progress);
	}

	private void doTranslate(double progress) {
		Double currentPosition = getDoubleFromPixel(element.getStyle().getMarginLeft());
		element.getStyle().setMarginLeft(currentPosition - currentPosition * progress, Unit.PX);

	}

	public Double getDoubleFromPixel(String input) {
		String numberOnly = input.replaceAll("[^0-9\\.]+", "");
		return Double.parseDouble(numberOnly);
	}

}
