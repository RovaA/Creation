package mg.rova.creation.client;

import com.google.gwt.dom.client.Element;

public class FadeAnimation extends HandledAnimation {

	public FadeAnimation(Element element, AnimationHandler handler) {
		super(element, handler);
	}

	@Override
	public void init() {

	}

	@Override
	protected void onUpdate(double progress) {
		element.getStyle().setOpacity(progress / 2);
	}

}
