package mg.rova.creation.client;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Display;

public abstract class HandledAnimation extends Animation {

	protected Element element;
	protected AnimationHandler handler;

	public HandledAnimation(Element element, AnimationHandler handler) {
		this.element = element;
		this.handler = handler;

		if (this.handler == null)
			return;
		this.handler.addAnimation(this);
		this.element.getStyle().setDisplay(Display.NONE);
	}

	public void animate(int duration) {
		this.element.getStyle().setDisplay(Display.BLOCK);
		init();
		run(duration);
	}

	public abstract void init();

	@Override
	protected void onComplete() {
		super.onComplete();
		if (handler != null)
			handler.nextAnimation();
	}

}
